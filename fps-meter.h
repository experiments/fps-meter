/*
 * fps-meter - Some functions to measure frames per seconds
 *
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FPS_METER_H
#define FPS_METER_H

#include <stdio.h>
#include <string.h>
#include <time.h>

#ifdef DEBUG
#define fps_meter_dbg(...)                     \
	do {                         \
		printf(__VA_ARGS__); \
		printf("\n");        \
		fflush(stdout);      \
	} while(0)
#else
#define fps_meter_dbg(...) do {} while(0)
#endif

#define NSEC_PER_SEC 1000000000

#define fps_meter_timespecsub(a, b, result)                                \
	do {                                                     \
		(result)->tv_sec = (a)->tv_sec - (b)->tv_sec;    \
		(result)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec; \
		if ((result)->tv_nsec < 0) {                     \
			--(result)->tv_sec;                      \
			(result)->tv_nsec += 1000000000;         \
		}                                                \
	} while(0)

struct fps_meter_stats {
	struct timespec time_start;
	struct timespec time_end;

	unsigned int frames;
	double nsecs;
};

static void fps_meter_init(struct fps_meter_stats *stats)
{
	memset(stats, 0, sizeof(*stats));

	clock_gettime(CLOCK_MONOTONIC, &stats->time_start);
	fps_meter_dbg("Init time: s: %ld, ns: %ld", stats->time_start.tv_sec, stats->time_start.tv_nsec);
}

static void fps_meter_update(struct fps_meter_stats *stats)
{
	struct timespec elapsed;

	fps_meter_dbg("Start time: s: %ld, ns: %ld", stats->time_start.tv_sec, stats->time_start.tv_nsec);

	clock_gettime(CLOCK_MONOTONIC, &stats->time_end);
	fps_meter_dbg("End time: s: %ld, ns: %ld", stats->time_end.tv_sec, stats->time_end.tv_nsec);

	fps_meter_timespecsub(&stats->time_end, &stats->time_start, &elapsed);
	fps_meter_dbg("Elapsed s: %ld ns: %ld", elapsed.tv_sec, elapsed.tv_nsec);

	stats->frames++;
	stats->nsecs += (elapsed.tv_sec * NSEC_PER_SEC + elapsed.tv_nsec);
	if (stats->nsecs >= NSEC_PER_SEC) {
		/* 
		 * if we were garanteed that each frame took less than
		 * a second, then just printing 'frames' would be enough here,
		 * but if we want to cover the case when a frame may take more
		 * than a second, some calculations have to be done.
		 */
		float fps = stats->frames / (stats->nsecs / NSEC_PER_SEC);
		printf("(frames: %d, nsecs: %f) FPS: %.2f\n", stats->frames, stats->nsecs, fps);
		stats->nsecs = 0;
		stats->frames = 0;
	}

	/* update the stats for the next iteration */
	clock_gettime(CLOCK_MONOTONIC, &stats->time_start);
}
#endif /* FPS_METER_H */
