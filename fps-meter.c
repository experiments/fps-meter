/*
 * fps-meter - Example program about how to measure frames per seconds
 *
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fps-meter.h"

int main(void)
{
	struct fps_meter_stats stats;
	struct timespec tmp;

	fps_meter_init(&stats);
	while (1) {
		/* simulate some workload of about 60fps */
		fps_meter_dbg("Doing some work, which takes about %d ns", NSEC_PER_SEC / 60);
		tmp.tv_sec = 0;
		tmp.tv_nsec = NSEC_PER_SEC / 60;
		clock_nanosleep(CLOCK_MONOTONIC, 0, &tmp, NULL);

		fps_meter_update(&stats);
	}

	return 0;
}
